<?php
/**
 * Plugin Name: dkzr NO oEmbed Discover
 * Plugin URI: https://dkzr.nl
 * Update URI: https://api.dkzr.nl/wp/update-check/
 * Description: Disable the oEmbed discover option of the REST API. https://wordpress.org/support/topic/paste-links-in-gutenberg-without-embedding/ en https://wordpress.org/support/article/embeds/#does-this-work-with-any-url
 * Author: Joost de Keijzer
 * Version: 2.0.1
 */
add_filter( 'oembed_linktypes', '__return_empty_array', 999999 );
